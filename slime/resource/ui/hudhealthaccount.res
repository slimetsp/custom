"Resource/UI/HudHealthAccount.res"
{
	"CHealthAccountPanel"
	{
		"fieldName"				"CHealthAccountPanel"
		"delta_item_x"			"20"
		"delta_item_start_y"	"20"
		"delta_item_end_y"		"20"
		"PositiveColor"			"Health Buff"
		"NegativeColor"			"Health Low"
		"delta_lifetime"		"2"
		"textAlignment"	        "west"
		"delta_item_font"		"nüMedium20"
	}
}