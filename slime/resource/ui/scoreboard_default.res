"Resource/UI/Scoreboard.res"
{
	"BlueScoreBG"
	{
		"ypos_minmode"		"167"
	}
	"RedScoreBG"
	{
		"ypos_minmode"		"167"
	}
	"RedTeamLabelWorking"
	{
		"pin_to_sibling"	"RedScoreBG"
		"pin_corner_to_sibling"	"PIN_TOPRIGHT"
		"pin_to_sibling_corner"	"PIN_TOPRIGHT"
	}
	"RedTeamScore"
	{
		"pin_to_sibling"	"REDScoreBG"
		"pin_corner_to_sibling"	"PIN_TOPLEFT"
		"pin_to_sibling_corner"	"PIN_TOPLEFT"
	}
	"RedTeamPlayerCount"
	{
		"pin_to_sibling"	"REDScoreBG2"
		"pin_corner_to_sibling"	"PIN_TOPRIGHT"
		"pin_to_sibling_corner"	"PIN_TOPRIGHT"
	}
	"TopBarBG"
	{
		"ypos_minmode"	"133"
	}
	"LocalPlayerStatsPanel"
	{
		"ypos_minmode"		"289"
	}
}