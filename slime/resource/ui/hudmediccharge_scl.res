"Resource/UI/HudMedicCharge.res"
{	
	"SmallChargeLabel"
	{
		"ControlName"	"CExLabel"
		"fieldName"		"SmallChargeLabel"
		"xpos"			"c-13"
		"ypos"			"c7"
		"zpos"			"2"
		"wide"			"26"
		"tall"			"12"
		"autoResize"	"1"
		"pinCorner"		"2"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"	"0"
		"labelText"		"#TF_UberchargeMinHUD"
		"textAlignment"	"center"
		"dulltext"		"0"
		"brighttext"	"0"
		"font"			"nü10"
		"fgcolor"		"0 240 0 255"
	}
}