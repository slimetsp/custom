
// Select a font to use
//------------------------------------------

	//#base "scheme/fonts_roboto.res"
	#base "scheme/fonts_Noto.res"

// Do not change these
//------------------------------------------

	#base "scheme/basesettings.res"
	#base "scheme/borders.res"
	#base "scheme/colors.res"
	#base "scheme/crosshairs.res"
	#base "scheme/symbols.res"

//------------------------------------------

Scheme
{
	CustomFontFiles
	{
		"1" "resource/tf.ttf"
		"2" "resource/tfd.ttf"
		"3"
		{
			"font" "resource/TF2.ttf"
			"name" "TF2"
		}
		"4" 
		{
			"font" "resource/TF2Secondary.ttf"
			"name" "TF2 Secondary"
		}
		"5" 
		{
			"font" "resource/TF2Professor.ttf"
			"name" "TF2 Professor"
		}
		"6" 
		{
			"font" "resource/TF2Build.ttf"
			"name" "TF2 Build"
		}
		"7"
		{
			"font" "resource/fonts/Blocks.ttf"
			"name" "Blocks"
		}
		"8"
		{
			"font" "resource/fonts/Cabin-Regular.otf"
			"name" "Cabin-Regular"
		}
		"9"
		{
			"font" "resource/fonts/Cabin-Medium.otf"
			"name" "Cabin-Medium"
		}
		"10"
		{
			"font" "resource/fonts/Cabin-Bold.otf"
			"name" "Cabin-Bold"
		}
		"11"
		{
			"font"	"resource/fonts/KnucklesCrosses.ttf"
			"name"	"KnucklesCrosses"
		}
		"12"
		{
			"font" "resource/fonts/Roboto-Regular.ttf"
			"name" "Roboto"
		}
		"13"
		{
			"font" "resource/fonts/Roboto-Medium.ttf"
			"name" "Roboto Medium"
		}
		"14"
		{
			"font" "resource/fonts/Roboto-Bold.ttf"
			"name" "Roboto Bold"
		}
		"14"
		{
			"font" "resource/fonts/Noto Sans Bold.ttf"
			"name" "Noto Sans Bold"
		}
		
	}
}